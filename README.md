# [FitNesse Integrated with Spring](http://fitnesse.org/) 

Welcome to SeyChelles, the FItNesse wiki integrated with the Spring Framework.  The purpose of this project is to run the FitNesse testing framework on a TomCat instance and also utilize the security frameworks that are available with Spring. 



## Quick start

* The majority of the new classes that were created to use FitNesse are located in the fitnesseMain package.  There were numerous modifications made to many other libraries (more documentation to come)



## Building the app

Gradle is recommended to build the application.  To build the standalone jar files needed to run the FitNesse instance, type

gradle standalone-jar clean build

