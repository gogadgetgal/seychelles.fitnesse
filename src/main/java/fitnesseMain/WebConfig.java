package fitnesseMain;

//import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

//import fitnesse.FitNesseContext;

@Configuration
@EnableWebMvc
public class WebConfig extends WebMvcConfigurerAdapter {

	//@Autowired	
	//private FitNesseContext contextconfig;
	
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
    
    	  
    	  registry.addResourceHandler("/files/fitnesse/**")
    	             .addResourceLocations("classpath:/**");
    	         // .addResourceLocations("http://localhost/templates/**");
    	        //      .setCachePeriod(31556926);
    }

}

