package fitnesseMain;


import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;
//import java.util.logging.Level;
import org.apache.commons.logging.*;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties.Hibernate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import util.StreamReader;

//import com.fasterxml.jackson.databind.ObjectMapper;

import fitnesse.FitNesseContext;
import fitnesse.FitSpringExpediter;
import fitnesse.http.ChunkedResponse;
import fitnesse.http.ChunkedDataProvider;
//import fitnesse.Responder;
//import fitnesse.http.MockRequest;
//import fitnesse.http.Request;
import fitnesse.http.Response;
import fitnesse.http.ResponseSender;
import fitnesse.http.SimpleResponse;
import fitnesse.http.UploadedFile;
//import fitnesse.http.SimpleResponse;
//import fitnesse.responders.files.FileResponder;
//import fitnesse.testutil.FitNesseUtil;
import fitnesse.util.MockSocket;




@Controller
public class fitNesseController   {
	
	
	private static final Log LOG = LogFactory.getLog(fitNesseController.class.getName());
	private Boolean flushOutput = true;
	private String EOL = "0\r\n" ;
	//private Response fitResponse;
	
	@Autowired
	private  HttpServletRequest request;
	
	@Autowired
	private  FitNesseContext contextconfig;
	
		
  // @RequestMapping(value = "/", produces = "text/html; charset=utf-8", method=RequestMethod.GET)
	@RequestMapping(value = "/",  method=RequestMethod.GET)
	public void mainentry( HttpServletResponse response) throws Exception {
	  //return   "Hi";
	  	
	  	response.sendRedirect(contextconfig.contextRoot + "FrontPage");
	   // return response;
    }

//	@RequestMapping(value = "/files:.+")
//	public void getFiles (@RequestBody String body ,  HttpServletResponse response) {
//			int port = request.getServerPort();
//		
//	       response.setContentType ("text/html; charset=utf-8");
//		   FitSpringExpediter expediter = getExpediter (port, response,  body);
//		 //  response.setHeader("Transfer-Encoding", "chunked");
//		   expediter.start();
//    }

 
	@RequestMapping(value={"/","/{pagename:.+}", "/{pagename:.+}/"}) 
    public void getWikiPage (@RequestBody String body, @PathVariable("pagename") String page,  HttpServletResponse response) throws Exception { 
		//getFileContentInfo();
		int port = request.getServerPort();	
      // LOG.info("Fitnesse Controller - " + page);
     //  page.contains("files") ? "files"
       response.setContentType ("text/html; charset=utf-8");
       FitSpringExpediter expediter = getExpediter (port, response, body);
      
	   expediter.start();
	  // responseWriter.w
	  
  	} 
	
	
//	@RequestMapping(value={"/","/{pagename:.+}", "/{pagename:.+}/"},  method=RequestMethod.POST) 
//    public void saveWikiPage (@RequestBody String body, @PathVariable("pagename") String page,  HttpServletResponse response) throws Exception { 
//		LOG.info("POST Request: " + page);
//		int port = request.getServerPort();	
//     
//       response.setContentType ("text/html; charset=utf-8");
//       FitSpringExpediter expediter = getExpediter (port, response, body);
//      
//	   expediter.start();
//
//	  
//  	} 
//	
//  	
	
	@RequestMapping(value="/{pagename:.+}", params = "test", method=RequestMethod.GET) 
	public void runTest ( @RequestBody String body,  @PathVariable("pagename") String page, HttpServletResponse response) throws Exception { 
		int port = request.getServerPort();
	
       response.setContentType ("text/html; charset=utf-8");
	   FitSpringExpediter expediter = getExpediter (port, response,  body);
	   response.setHeader("Transfer-Encoding", "chunked");
	   expediter.start();
	  // expediter.close();
	  	
  	} 
   
	
	@RequestMapping(value="/{pagename:.+}", params = "whereUsed", method=RequestMethod.GET) 
	public void whereUsed ( @RequestBody String body,@PathVariable("pagename") String page,  HttpServletResponse response) throws Exception { 
		int port = request.getServerPort();
	  // response.se
      // LOG.info("Fitnesse Test - whereUsed" );
     
       response.setContentType ("text/html; charset=utf-8");
	   //output = response.getOutputStream();
       FitSpringExpediter expediter = getExpediter (port, response, body);
	   response.setHeader("Transfer-Encoding", "chunked");

	   expediter.start();

	  	
  	} 
  

   
   
   private  Map<String, Object> getParmsInfo() {
	  // String nParm[];
	   Map<String, Object> parmsRemap = new HashMap<String, Object>();
	   Map<String, String[]> parms = request.getParameterMap();
	  // String sValue = "";
	  
	   for (String key : parms.keySet()) {
		  
		  
		 String[] nParm = parms.get(key);
		   for (String parm : nParm) {
			   parmsRemap.put(key, parm);
		   		LOG.info("Key:" + key + ",  Value: " + nParm[0]);
		   }
//		
		  
	   	}
		
	   return parmsRemap;
			
			
   }
   
   
   
 //get user agent
 	private String getUserAgent() {
 		return request.getHeader("user-agent");
 	}
  
 	//get request headers
 	private Map<String, Object> getHeadersInfo() {
  
 		Map<String, Object> map = new HashMap<String, Object>();
    
 		@SuppressWarnings("rawtypes")
		Enumeration headerNames = request.getHeaderNames();
 		while (headerNames.hasMoreElements()) {
 			String key = (String) headerNames.nextElement();
 			String value = request.getHeader(key);
 			//LOG.info("Header Key = " + key + "/  Header Value = " + value);
 			map.put(key, value);
 		}
  
 		return map;
 	}

   private FitSpringExpediter getExpediter (int port, HttpServletResponse res, String body) {
	   
	   try {
		   		FitSpringExpediter expediter = new FitSpringExpediter(getSocket(port), request, res, contextconfig,
				   body, getParmsInfo(), getHeadersInfo(), false);
		
		   			return expediter;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	   return null;
   }
   

	  public Socket getSocket(int port) {
	  
   	
		  Socket socket  = new MockSocket();
		  // ServerSocket welcomeSocket = new ServerSocket(6784);
		   SocketAddress me = new InetSocketAddress("127.0.0.1", port);
		  // socket = s;
		   try {
			   socket.connect(me);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		  // output = response.getOutputStream();
		    return socket;
	  }

  
	
	
	
}