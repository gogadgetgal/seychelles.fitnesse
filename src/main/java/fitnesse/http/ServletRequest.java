// Copyright (C) 2003-2009 by Object Mentor, Inc. All rights reserved.
// Released under the terms of the CPL Common Public License version 1.0.
package fitnesse.http;

import fitnesse.ContextConfigurator;
import fitnesse.util.Base64;
import fitnesseMain.FitNesseMain;
import fitnesseMain.fitNesseController;
import util.StreamReader;
import java.io.*;
import java.util.Enumeration;
import java.util.Map;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServletRequest extends Request {
 
	private static  HttpServletRequest httpServletReq;
   
    private static final Logger LOG = Logger.getLogger(ServletRequest.class.getName());
	
  

  public ServletRequest(HttpServletRequest res, String contextRoot) {
	  super.setResource("");
	  super.setContextRoot(contextRoot);
	  httpServletReq = res;
	 
  }
  

  private RuntimeException parseException = null;

  
  public void handleRequest(HttpServletRequest request) throws IOException {

//		PrintWriter out = res.getWriter();
//		res.setContentType("text/plain");
	  Map<String, String[]> parameters = request.getParameterMap();

	    for(String key : parameters.keySet()) {
	        System.out.println(key);
	        String[] vals = parameters.get(key);
	        for(String val : vals)
	            LOG.info(" -> " + val);
	    }

		

	}
  public void setRequestUri(String value) {
    requestURI = value;
  }

  public void setRequestLine(String value) {
    requestLine = value;
  }

  public void setBody(String value) {
    entityBody = value;
  }

  public void setQueryString(String value) {
    queryString = value;
    parseQueryString(value);
  }

  public void addInput(String key, Object value) {
    inputs.put(key, value);
  }

  public void addHeader(String key, Object value) {
    headers.put(key.toLowerCase(), value);
  }

  public void throwExceptionOnParse(RuntimeException e) {
    parseException = e;
  }

  public void getCredentials() {
  }


  
  public void setCredentials(String username, String password) {
    authorizationUsername = username;
    authorizationPassword = password;

  }

  public void parse() {
    if (parseException != null) {
      throw parseException;
    }
  }
}
