package fitnesse.http;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.SocketException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import org.apache.tools.ant.util.FileUtils;

import util.Clock;
import util.StreamReader;




public class SpringMockRequest extends Request {
  private RuntimeException parseException = null;
  private final HttpServletRequest request;
  private String requestBody;
  private String contentType;
  private static final Pattern headerPattern = Pattern.compile("([^:]*): (.*)");
  private static final Pattern boundaryPattern = Pattern
  .compile("boundary=(.*)");
  private static final Pattern multipartHeaderPattern = Pattern
  .compile("([^ =]+)=\\\"([^\"]*)\\\"");
  //private String page;
 
  //protected Map<String, String[]> inputs = new HashMap<String,String[]>();
 // protected Map<String, String> headers = new HashMap<String, String>();
	
  public SpringMockRequest(HttpServletRequest request, Map<String, Object> params, String requestBody, 
		                         Map<String, Object> headers) {
	  
    super.setResource("");
    this.request = request;
    this.inputs = params;
    this.headers = headers;
   
  }
  
 
  public void setRequestUri(String value) {
    requestURI = value;
  }

  public void setRequestLine(String value) {
    requestLine = value;
  }

  public void setContentType(String contentType){
	  
	  addUniqueInputString("format", contentType);
  }
  public void setBody(String value) {
    entityBody = value;
  }

  public void setQueryString(String value) {
    queryString = value;
  
  }

  public void addInput(String key, String value) {
	  addUniqueInputString (key, value) ;
   //.put(key, value)
  }


  public void addHeader(String key, String value) {
    headers.put(key.toLowerCase(), value);
  }

  public void throwExceptionOnParse(RuntimeException e) {
    parseException = e;
  }

  public void getCredentials() {
  }

  public void setCredentials(String username, String password) {
    authorizationUsername = username;
    authorizationPassword = password;

  }

  @Override
  public void parse() {
	 parseQueryString(requestBody); 
    if (parseException != null) {
      throw parseException;
    }
    setHasBeenParsed(true);
  }
  
  private void getMulitPartFileContentInfo() throws IllegalStateException, IOException, ServletException {
	  
	 // ClassLoader classLoader = SpringMockRequest.class.getClassLoader();
	   // File classpathRoot = new File(classLoader.getResource("").getPath());
      //String classpath = classpathRoot.getPath();
	    //return ;
	//  String tempFileName = "_fitnesse.tmp";
	  Object value = null;
	  String name = "";
	  String contentType = request.getHeader("Content-Type");
      if (contentType != null && contentType.startsWith("multipart/form-data")) {
		  for (Part part : request.getParts()) {
			  String contentDisp = part.getHeader("content-disposition");
			
			
			  String contenttype = part.getHeader("Content-Type");
			 
		        System.out.println("content-disposition header= "+contentDisp);
		        String[] tokens = contentDisp.split(";");
		         value = null;
		         name = "";
		         String headerkey = "", headerval = "";
		        for (String token : tokens) {
		        	
		        	token = token.trim();
		        	if (token.indexOf("name")==0) {
		        		String temp[] = token.split("=");
		        		if (temp.length > 1)
		        			name = temp[1].substring(1, temp[1].length()-1);
		        		}
		        		else  {
			        	
			        	    String temp[] = token.split("=");
			        	    if (temp.length > 1) {
			        	    	headerkey = temp[0];
			        	    	headerval = temp[1].substring(1, temp[1].length()-1);//token.substring(token.indexOf("=") + 2, token.length()-1).trim();
					           
			        	    }
			        	  }	
		        	}
		        	
    	    		//File tempFile = new File(tempFileName);
		         	if (headerkey.equals("filename")) {
		         		part.write("c:\\temp\\FitNesse.uploadedFile");	    		
		            	value = new UploadedFile(headerval, contentType, new File("c:\\temp\\FitNesse.uploadedFile"));
		            	
		            }
		         	else {
		         		//part.write("");
		         		value = "upload";
		         	}
		         		
		         	inputs.put(name, value);
		        	
		     
	
		  	} //all parts in request
      } //multi-form contenty
	  
  }
  
 
  
  
  

  @Override
  protected void parseQueryString(String reqBody) {
	   try {
		   input = new StreamReader(new BufferedInputStream(request.getInputStream()));
			setBody(reqBody);
			String query = request.getQueryString() != null ? request.getQueryString() : "";
			setQueryString(query);
			String uri = request.getRequestURI() != null ? request.getRequestURI() : "";
			setRequestUri(uri);
			try {
				getMulitPartFileContentInfo() ;
			} catch (ServletException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String resource = stripContextRoot(uri);
			setResource(resource );
			//setContextRoot(uri);
			
	   } catch (RuntimeException e) {
		   parseException = e;
	   } catch (IOException f) {
		   //
	   }
//	    }
	  }

  @Override
  public int getContentLength() {
	    return  Integer.parseInt((String) headers.get("Content-Length"));
	  }

}
