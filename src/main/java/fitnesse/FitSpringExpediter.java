package fitnesse;


import fitnesse.http.Request;
import fitnesse.http.Response;
import fitnesse.http.ResponseSender;
import fitnesse.http.SimpleResponse;
import fitnesse.http.SpringMockRequest;
import fitnesse.responders.ErrorResponder;
import fitnesse.util.MockSocket;
import fitnesseMain.fitNesseController;
import util.StringUtil;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Bean;

public class FitSpringExpediter extends FitNesseExpediter {
  private static final Log LOG = LogFactory.getLog(FitSpringExpediter.class.getName());

//  private final Socket socket;

  private SpringMockRequest request;
  private HttpServletRequest httpRequest;
  private HttpServletResponse httpResponse;
  //private Response response;
 // private final FitNesseContext context;
 // protected long requestParsingTimeLimit;
  private  String requestBody;
  private  String pageName;
  private Map<String, Object> params;
  private Map<String, Object> headers;
//  private final ResponseSender sender;
  private static Boolean sendHeader = false;
  private final OutputStream output;
 // private final  Socket s = getSocket();
  private static int port;

  public FitSpringExpediter(Socket s, HttpServletRequest request,  HttpServletResponse response,
		  FitNesseContext context,  String body,
		  Map<String, Object> parameters, Map<String, Object> headers, Boolean sendHeaders) throws IOException {
	
    super (s, context);
	
    this.output = response.getOutputStream();
    this.httpRequest = request;
    this.httpResponse = response;
    this.sendHeader = sendHeaders;
  //  setPage(page);
	setRequestBody(body);
	setParams(parameters);
	setHeaders(headers);
	port = context.port;
    requestParsingTimeLimit = 10000;
  }
  
  

//  public  void setPage(String pagename) {
//	 pageName = pagename;
//	  	  
//  }
  public void setHeaders(Map<String, Object> headers) {
	 
	  this.headers = headers;
  }
  
  public void setParams (Map<String, Object> parms) {
	 
	  this.params = parms;
  }
  
  public  void setRequestBody (String requestbody) {
	  requestBody = requestbody;
  }
  
  public Response getResponse() {
	  return response;
  }
  


  @Override
  public void start() {
	    try {
	      Request request = makeRequest();
	      makeResponse(request);
	      sendResponse();
	    }
	    catch (SocketException se) {
	      // can be thrown by makeResponse or sendResponse.
	    }
	    catch (Throwable e) {
	      LOG.warn( "Unexpected exception", e);
	    }
	  }

  
@Override
  public Request makeRequest() {
    request = new SpringMockRequest(httpRequest, params, requestBody,  headers);
	
    request.setContextRoot(context.contextRoot);
    return request;
  }




  @Override
  public void sendResponse() throws IOException {
	
    response.setHttpHeaders(sendHeader);
    updateHeaders(response.getHeaders(), httpResponse);
    response.sendTo(this);
 
  }

 @Override
  protected Response makeResponse(Request request) throws SocketException {
	    try {
	    
          request.parse();
      
	      if (!hasError) {
	        if (context.contextRoot.equals(request.getRequestUri() + "/") ||
	        		request.getRequestUri().equals("/")) {
	          response = new SimpleResponse();
	          response.redirect(context.contextRoot, "");
	        } else {
	          response = createGoodResponse(request);
	        }
	      }
	    }
	    catch (SocketException se) {
	      throw se;
	    }
	    catch (Exception e) {
	      LOG.warn( "Unable to handle request", e);
	      response = new ErrorResponder(e).makeResponse(context, request);
	    }
	    
	   
	    return response;
  }
  
 
 @Override
	public void send(byte[] bytes) {
   	
	    try {
	    
	        if (bytes != null)
	        	output.write(bytes);
	    	output.flush();
	  
	    }
	    catch (IOException e) {
	      LOG.info( "Output stream closed unexpectedly (Stop button pressed?)", e);
	    }
	  }

     @Override
	  public void close() {
	    try {
	     // log(socket, request, response);
	     
	      socket.close();
	      output.close();
	    }
	    catch (IOException e) {
	      LOG.warn("Error while closing socket", e);
	    }
	  }
     
     
     private void updateHeaders(HashMap<String, String> updated, HttpServletResponse res ){
  	   
  	   
  	   for (String key : updated.keySet()) {
  		   String nParm = updated.get(key);
  		   if (nParm != null) {
  			   String sValue = updated.get(key);
  			   res.setHeader(key, sValue);
  		   }
  	   }
  	   	

		res.setHeader("Server", "FitSpringServer-" + context.version);
		res.setHeader("Connection", "close");

  	   
  		res.setStatus(response.getStatus());
     }
}


